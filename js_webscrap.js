//necessita instalação previa dos pacotes axios, cheerio e playwright (npm install axios cheerio playwright)
//fonte de estudos: https://www.zenrows.com/blog/web-scraping-with-javascript-and-nodejs#scraping-the-basics
//o script é assincrono, o download dos arquivos corre em uma thread diferente da execução do script

let axios = require('axios'); //variavel que recebe a funcao que obtem o codigo html da página
let cheerio = require('cheerio'); //variavel que recebe a funcao que processa o codigo html da pagina
let https = require('https'); //variavel que recebe a funcao que realiza consultas https
let http = require('http'); //variavel que recebe a funcao que realiza consultas http
let fs = require('fs'); //variavel que recebe a funcao manipula e gera arquivos fisicos

let url = "https://mirrors.fy1m.com/h_tools/"; //url a ser processada
let extensao = ".msi"; //tipo de arquivo a ser baixado da pagina

axios.get(url) //funcao que obtem e processa o codigo html
	.then(({ data }) => { 
	let buscaLinks = cheerio.load(data);
	let links = obtemLinks(buscaLinks);
	salvaArquivos(links);
});
	
let obtemLinks = buscaLinks => [ //variavel que processa o html e transforma o resultado em um array
	...new Set( 
		buscaLinks('a') //abre todas as tags <a>
			.map((_, a) => buscaLinks(a).attr('href')) //carrega o conteudo de todos os atributos 'href' das tags <a>
			.toArray() //converte os dados obtidos em um array
	), 
];

function salvaArquivos(dados){ //funcao que processa o array obtido e gera os arquivos a partir dele
	for(let i = 0; i < dados.length; i++){
		if(dados[i].indexOf(extensao) !== -1){ //verifica se o arquivo na pagina tem a extensao informada
			console.log("Processando o arquivo numero "+i+": "+dados[i]);
			let file = fs.createWriteStream("js_"+dados[i]);
			if(url.indexOf("https") !== -1){ //verifica se a url é https ou http
				let request = https.get(url+dados[i], function(response) {
					response.pipe(file);
				});
			}else{
				let request = http.get(url+dados[i], function(response) {
					response.pipe(file);
				});
			}
		}else{
			console.log("O arquivo numero "+i+" não possui a extensao \""+extensao+"\"");
		}
	}
}